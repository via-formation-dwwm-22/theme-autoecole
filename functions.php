<?php
function cptui_register_my_cpts_formateur() {

    /**
     * Post Type: formateurs.
     */

    $labels = [
        "name" => __( "formateurs", "custom-post-type-ui" ),
        "singular_name" => __( "formateur", "custom-post-type-ui" ),
        "menu_name" => __( "Formateurs", "custom-post-type-ui" ),
        "all_items" => __( "Tous les formateurs", "custom-post-type-ui" ),
        "add_new" => __( "Ajouter un nouveau", "custom-post-type-ui" ),
        "add_new_item" => __( "Ajouter un nouveau formateur", "custom-post-type-ui" ),
        "edit_item" => __( "Modifier formateur", "custom-post-type-ui" ),
        "new_item" => __( "Nouveau formateur", "custom-post-type-ui" ),
        "view_item" => __( "Voir formateur", "custom-post-type-ui" ),
        "view_items" => __( "Voir formateurs", "custom-post-type-ui" ),
        "search_items" => __( "Recherche de formateurs", "custom-post-type-ui" ),
        "not_found" => __( "Aucun formateurs trouvé", "custom-post-type-ui" ),
        "not_found_in_trash" => __( "Aucun formateurs trouvé dans la corbeille", "custom-post-type-ui" ),
        "parent" => __( "formateur parent :", "custom-post-type-ui" ),
        "featured_image" => __( "Image mise en avant pour ce formateur", "custom-post-type-ui" ),
        "set_featured_image" => __( "Définir l’image mise en avant pour ce formateur", "custom-post-type-ui" ),
        "remove_featured_image" => __( "Retirer l’image mise en avant pour ce formateur", "custom-post-type-ui" ),
        "use_featured_image" => __( "Utiliser comme image mise en avant pour ce formateur", "custom-post-type-ui" ),
        "archives" => __( "Archives de formateur", "custom-post-type-ui" ),
        "insert_into_item" => __( "Insérer dans formateur", "custom-post-type-ui" ),
        "uploaded_to_this_item" => __( "Téléverser sur ce formateur", "custom-post-type-ui" ),
        "filter_items_list" => __( "Filtrer la liste de formateurs", "custom-post-type-ui" ),
        "items_list_navigation" => __( "Navigation de liste de formateurs", "custom-post-type-ui" ),
        "items_list" => __( "Liste de formateurs", "custom-post-type-ui" ),
        "attributes" => __( "Attributs de formateurs", "custom-post-type-ui" ),
        "name_admin_bar" => __( "formateur", "custom-post-type-ui" ),
        "item_published" => __( "formateur publié", "custom-post-type-ui" ),
        "item_published_privately" => __( "formateur publié en privé.", "custom-post-type-ui" ),
        "item_reverted_to_draft" => __( "formateur repassé en brouillon.", "custom-post-type-ui" ),
        "item_scheduled" => __( "formateur planifié", "custom-post-type-ui" ),
        "item_updated" => __( "formateur mis à jour.", "custom-post-type-ui" ),
        "parent_item_colon" => __( "formateur parent :", "custom-post-type-ui" ),
    ];

    $args = [
        "label" => __( "formateurs", "custom-post-type-ui" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "delete_with_user" => false,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "can_export" => true,
        "rewrite" => [ "slug" => "formateur", "with_front" => true ],
        "query_var" => true,
        "menu_position" => 5,
        "menu_icon" => "dashicons-welcome-learn-more",
        "supports" => [ "title", "editor", "thumbnail", "excerpt", "trackbacks", "custom-fields", "comments", "revisions", "author", "page-attributes", "post-formats" ],
        "show_in_graphql" => false,
    ];

    register_post_type( "formateur", $args );
}
add_action( 'init', 'cptui_register_my_cpts_formateur' );


