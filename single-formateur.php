<?php
get_header(); ?>

<main>
    <?php if (have_posts()){
        while (have_posts()){
            the_post();?>
            <section class="formateur-infos">
                <div>
                    <h1><?= get_field("lname"); ?></h1>
                    <h2><?= get_field("fname"); ?></h2>
                    <p>Date d’obtention du permis : <?= get_field("license_date"); ?></p>
                    <p>Date de début d’activité de formateur : <?= get_field("training_date"); ?></p>

                    <?php
                    $specialties = [];
                    foreach(get_field("specialty") as $specialty){
                        $specialties[] = $specialty['label'];
                    }
                    echo implode(", ", $specialties); // affichage du contenu du tableau avec PHP natif

                    //echo implode(", ", wp_list_pluck(get_field("specialty"), "label")); //affichage du ccintenu du même tableau avec des fonctions WP
                    ?>

                    <?php if (get_field("email")) { ?>
                    <p>Vous souhaitez entrer en contact avec ce formateur, envoyez lui un email :
                        <a href="mailto:<?= get_field("email"); ?>?subject=Bonjour"><?= get_field("email"); ?></a>
                    </p>
                    <?php } ?>
                </div>
                <div>
                    <?php the_post_thumbnail() ?>
                </div>
            </section>
            <section>
                <h2>La petite description du Formateur</h2>
                <?php the_field("bio");?>
            </section>
            <section>
                <h2>Ses résultats</h2>
                <p>Ce formateur vous communique le taux de réussite de ses élèves depuis son arrivée à l’Auto-école du Coin :</p>
                <?php the_field("success_rate"); ?>
            </section>

        <?php }
    } ?>
</main>

<?php get_footer();
