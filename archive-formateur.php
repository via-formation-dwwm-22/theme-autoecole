<?php
get_header(); ?>

<main>
    <div class="archive-container">
        <h1>Découvrez notre équipe de professionnels</h1>
        <p>A l’auto-école du coin, nous sélectionnons les meilleurs profils pour vous assurer un passage de permis de qualité.</p>
        <p>Découvrez notre équipe dédiée et dévouée.</p>

        <?php
        if (have_posts()){ ?>
            <section class="formateur-container">
                <?php while (have_posts()){
                    the_post(); ?>
                    <article>
                        <a href="<?= get_the_permalink(); ?>">
                            <?= get_the_post_thumbnail( get_the_ID() ,[300,200]) ?>
                        </a>
                        <a href="<?= get_the_permalink(); ?>">
                            <h2>
                                <?= get_the_title(); ?>
                            </h2>
                        </a>
                        <a href="<?= get_the_permalink(); ?>">
                            <p>Plus d'infos</p>
                        </a>
                    </article>
                <?php } ?>
            </section>
        <?php }
        else { ?>
            <p><em>Navré. Nous ne disposons pas de formateurs actuellement.</em></p>
        <?php }
        ?>
    </div>
</main>

<?php get_footer();
